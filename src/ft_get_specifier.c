/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_specifier.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:29:16 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 15:04:54 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_get_specifier(const char *format, t_printf *env)
{
	int		i;
	char	c;

	i = 0;
	c = '\0';
	while (SPECIFIER_STR[i])
	{
		if (format[env->i] == SPECIFIER_STR[i])
		{
			c = format[env->i];
			break ;
		}
		++i;
	}
	if (!SPECIFIER_STR[i] && !c)
	{
		env->backup = env->i;
		env->specifier = format[env->i];
		return ;
	}
	env->specifier = c;
	if (!env->qualifier)
		env->qualifier = 'd';
}
