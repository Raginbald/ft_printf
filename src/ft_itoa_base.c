/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:31:09 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 11:31:10 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <limits.h>
#include "libftprintf.h"

void	ft_init_var_itoa(t_itoa *var, int len, long num)
{
	if (!(var->buffer = malloc(sizeof(char) * len + 1)))
		ft_exitp(2, ERROR_MALLOC);
	var->result = 0;
	var->divisor = 1;
	var->i = 0;
	if (num < 0)
	{
		if (num == LONG_MIN)
			var->nbr = (unsigned long)num;
		else
		{
			num = -num;
			var->nbr = (unsigned long)num;
		}
	}
	else
		var->nbr = (unsigned long)num;
}

char	*ft_s_itoa_base(t_printf *env, long num, int base)
{
	t_itoa	var;

	ft_init_var_itoa(&var, env->len, num);
	while ((var.nbr / var.divisor) >= (unsigned long)base)
		var.divisor *= base;
	while (var.divisor > 0)
	{
		var.result = (var.nbr / var.divisor) % (unsigned long)base;
		var.buffer[var.i] = env->base_str[var.result];
		++var.i;
		var.divisor /= base;
	}
	var.buffer[var.i] = '\0';
	return (var.buffer);
}

char	*ft_u_itoa_base(t_printf *env, unsigned long num, int base)
{
	unsigned long	result;
	unsigned long	divisor;
	int				i;
	char			*buffer;

	divisor = 1;
	i = 0;
	if (!(buffer = malloc(sizeof(char) * env->len + 1)))
		ft_exitp(2, ERROR_MALLOC);
	while ((num / divisor) >= (unsigned long)base)
		divisor *= base;
	while (divisor > 0)
	{
		result = (num / divisor) % base;
		buffer[i] = env->base_str[result];
		++i;
		divisor /= base;
	}
	buffer[i] = 0;
	return (buffer);
}
