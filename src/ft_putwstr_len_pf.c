/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwstr_len_pf.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/21 16:05:44 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 16:05:45 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_putwstr_len_pf(wchar_t *str, int len)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	if (str == NULL)
		return (0);
	while (len > 0 && str[j] && ft_wclen_pf(str[j]) <= len)
	{
		i += ft_putwchar(str[j]);
		++j;
		len -= i;
	}
	return (i);
}
