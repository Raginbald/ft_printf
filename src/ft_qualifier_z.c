/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_qualifier_z.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:33:55 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 11:33:57 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	ft_qualifier_z(t_printf *env, va_list args)
{
	size_t	snum;

	snum = 0;
	if (env->flag & FLAG_SNBR)
	{
		snum = va_arg(args, size_t);
		env->snum = snum;
	}
	else
		env->num = va_arg(args, size_t);
}
