/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstrp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/09 14:15:03 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:11:34 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int		ft_putstr_len(const char *str, int len)
{
	if (str == NULL)
		return (0);
	write(1, str, len);
	return (len);
}

int		ft_putstrp(const char *str)
{
	int	i;

	i = 0;
	if (str == NULL)
		return (0);
	i = ft_strlenp(str);
	write(1, str, i);
	return (i);
}
