/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_number_len.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 12:36:54 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 15:12:49 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int		ft_s_numberlen(long num, int base)
{
	int				len;
	unsigned long	divisor;
	unsigned long	nbr;

	len = 1;
	divisor = 1;
	nbr = (unsigned long)num;
	if (num < 0)
		nbr = (unsigned long)-num;
	if (nbr == 0)
		return (1);
	while ((nbr / divisor) >= (unsigned long)base)
	{
		divisor *= base;
		++len;
	}
	return (len);
}

int		ft_u_numberlen(unsigned long num, int base)
{
	int				len;
	unsigned long	divisor;

	len = 1;
	divisor = 1;
	if (num == 0)
		return (1);
	while ((num / divisor) >= (unsigned long)base)
	{
		divisor *= base;
		++len;
	}
	return (len);
}
