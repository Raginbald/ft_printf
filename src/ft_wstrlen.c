/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 12:42:14 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 12:42:30 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int		ft_wstrlen(const wchar_t *str)
{
	int len;

	len = 0;
	while (str[len])
		++len;
	return (len);
}
