/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_u.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:19:08 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 12:32:04 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_arg_u(t_printf *env, va_list args)
{
	env->flag |= FLAG_UNBR;
	ft_arg_needler(args, env, env->tab_qual, env->qualifier);
	env->len += ft_u_numberlen(env->num, env->base);
	env->str = ft_u_itoa_base(env, env->num, env->base);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}

void	ft_arg_uu(t_printf *env, va_list args)
{
	env->flag |= FLAG_UNBR;
	env->num = va_arg(args, unsigned long);
	env->len += ft_u_numberlen(env->num, env->base);
	env->str = ft_u_itoa_base(env, env->num, env->base);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}
