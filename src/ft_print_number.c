/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_number.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:49:45 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/23 13:38:01 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

static void		ft_print_number_4(t_printf *env)
{
	if (env->flag & FLAG_ALTERNATE)
	{
		if ((env->num != 0 && env->base == 8)
			|| (env->flag & FLAG_NO_PRECISION && env->base == 8))
		{
			env->printed += ft_putcharp('0');
			--env->precision;
		}
		if (env->base == 16)
		{
			env->printed += ft_putcharp('0');
			env->printed += ft_putcharp(env->base_str[16]);
		}
	}
}

static void		ft_print_number_3(t_printf *env, char *sign)
{
	if (env->flag & FLAG_ALTERNATE && (env->base == 8 || env->base == 16))
	{
		--env->width;
		if (env->base == 16)
			--env->width;
	}
	if (env->len > env->precision)
		env->precision = env->len;
	env->width -= env->precision;
	if (!(env->flag & (FLAG_ZERO + FLAG_LEFT)))
	{
		while (env->width-- > 0)
			env->printed += ft_putcharp(' ');
	}
	if (*sign)
		env->printed += ft_putcharp(*sign);
}

static void		ft_print_number_2(t_printf *env, char *sign)
{
	if (env->flag & FLAG_LEFT)
		env->flag &= ~FLAG_ZERO;
	if (env->flag & FLAG_SNBR)
	{
		if (env->snum < 0)
		{
			*sign = '-';
			--env->width;
		}
		else if (env->flag & FLAG_PLUS)
		{
			*sign = '+';
			--env->width;
		}
		else if (env->flag & FLAG_SPACE)
		{
			*sign = ' ';
			--env->width;
		}
	}
}

void			ft_print_number(t_printf *env)
{
	char	padchar;
	char	sign;

	padchar = (env->flag & FLAG_ZERO
				&& (env->width - env->precision) > env->precision) ? '0' : ' ';
	sign = 0;
	ft_print_number_2(env, &sign);
	ft_print_number_3(env, &sign);
	ft_print_number_4(env);
	if (!(env->flag & FLAG_LEFT))
	{
		while (env->width-- > 0)
			env->printed += ft_putcharp(padchar);
	}
	while (env->len < env->precision--)
		env->printed += ft_putcharp('0');
	env->printed += ft_putstrp(env->str);
	while (env->width-- > 0)
		env->printed += ft_putcharp(' ');
}
