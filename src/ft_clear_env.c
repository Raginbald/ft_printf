/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_clear_env.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 12:40:41 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 12:41:05 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_clear_env(t_printf *env)
{
	env->flag = 0;
	env->width = -1;
	env->precision = -1;
	env->qualifier = 0;
	env->specifier = 0;
	env->base = 10;
	env->base_str = BASE_STR;
	env->len = 0;
	if (env->str)
	{
		free(env->str);
		env->str = 0;
	}
	env->num = 0;
}
