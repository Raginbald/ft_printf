/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_argument.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:31:37 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 15:09:51 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	ft_print_argument(va_list args, t_printf *env, const char *format)
{
	env->backup = env->i;
	ft_get_flags(format, env);
	ft_get_width(args, format, env);
	ft_get_precision(args, format, env);
	ft_get_qualifier(format, env);
	ft_get_specifier(format, env);
	ft_arg_needler(args, env, env->tab_spec, env->specifier);
	ft_clear_env(env);
}
