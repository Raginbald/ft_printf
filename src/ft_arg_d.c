/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:03:58 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 12:30:24 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_arg_d(t_printf *env, va_list args)
{
	env->flag |= FLAG_SNBR;
	ft_arg_needler(args, env, env->tab_qual, env->qualifier);
	env->len += ft_s_numberlen(env->snum, env->base);
	env->str = ft_s_itoa_base(env, env->snum, env->base);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}

void	ft_arg_i(t_printf *env, va_list args)
{
	env->flag |= FLAG_SNBR;
	ft_arg_needler(args, env, env->tab_qual, env->qualifier);
	env->len += ft_s_numberlen(env->snum, env->base);
	env->str = ft_s_itoa_base(env, env->snum, env->base);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}

void	ft_arg_dd(t_printf *env, va_list args)
{
	env->flag |= FLAG_SNBR;
	env->snum = va_arg(args, long);
	if (env->snum < 0)
		env->flag |= FLAG_ISNEG;
	env->len += ft_s_numberlen(env->snum, env->base);
	env->str = ft_s_itoa_base(env, env->snum, env->base);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}
