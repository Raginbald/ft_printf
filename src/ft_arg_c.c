/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_c.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:02:36 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 11:18:00 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	ft_arg_c(t_printf *env, va_list args)
{
	char	padchar;

	padchar = (env->flag & FLAG_ZERO) ? '0' : ' ';
	if (env->qualifier == 'l')
		ft_arg_cc(env, args);
	else
	{
		if (!(env->flag & FLAG_LEFT))
		{
			while (--env->width > 0)
				env->printed += ft_putcharp(padchar);
		}
		env->printed += ft_putcharp((char)va_arg(args, int));
		while (--env->width > 0)
			env->printed += ft_putcharp(' ');
	}
}

void	ft_arg_cc(t_printf *env, va_list args)
{
	char	padchar;

	padchar = (env->flag & FLAG_ZERO) ? '0' : ' ';
	if (!(env->flag & FLAG_LEFT))
	{
		while (--env->width > 0)
			env->printed += ft_putcharp(padchar);
	}
	env->printed += ft_putwchar((wchar_t)va_arg(args, int));
	while (--env->width > 0)
		env->printed += ft_putcharp(' ');
}
