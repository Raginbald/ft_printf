/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_x.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:19:47 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 12:32:30 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_arg_x(t_printf *env, va_list args)
{
	env->flag |= FLAG_UNBR;
	env->base = 16;
	ft_arg_needler(args, env, env->tab_qual, env->qualifier);
	if (env->num == 0)
		env->flag &= ~FLAG_ALTERNATE;
	env->len += ft_u_numberlen(env->num, env->base);
	env->str = ft_u_itoa_base(env, env->num, env->base);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}

void	ft_arg_xx(t_printf *env, va_list args)
{
	env->flag |= FLAG_UNBR;
	env->base = 16;
	env->base_str = BASE_STR_UP;
	ft_arg_needler(args, env, env->tab_qual, env->qualifier);
	if (env->num == 0)
		env->flag &= ~FLAG_ALTERNATE;
	env->len += ft_u_numberlen(env->num, env->base);
	env->str = ft_u_itoa_base(env, env->num, env->base);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}
