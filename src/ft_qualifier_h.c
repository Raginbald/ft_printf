/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_qualifier_h.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:32:42 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 11:37:35 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	ft_qualifier_h(t_printf *env, va_list args)
{
	if (env->flag & FLAG_SNBR)
	{
		env->snum = (short)va_arg(args, int);
		if (env->snum < 0)
			env->flag |= FLAG_ISNEG;
	}
	else
		env->num = (unsigned short)va_arg(args, unsigned int);
}

void	ft_qualifier_hh(t_printf *env, va_list args)
{
	if (env->flag & FLAG_SNBR)
	{
		env->snum = (char)va_arg(args, int);
		if (env->snum < 0)
			env->flag |= FLAG_ISNEG;
	}
	else
		env->num = (unsigned char)va_arg(args, unsigned int);
}
