/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_qualifier_l.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:33:45 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 11:33:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	ft_qualifier_l(t_printf *env, va_list args)
{
	if (env->flag & FLAG_SNBR)
	{
		env->snum = va_arg(args, long);
		if (env->snum < 0)
			env->flag |= FLAG_ISNEG;
	}
	else
		env->num = va_arg(args, unsigned long);
}

void	ft_qualifier_ll(t_printf *env, va_list args)
{
	if (env->flag & FLAG_SNBR)
	{
		env->snum = va_arg(args, long);
		if (env->snum < 0)
			env->flag |= FLAG_ISNEG;
	}
	else
		env->num = va_arg(args, unsigned long);
}
