/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_precision.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:27:22 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 15:03:10 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_get_precision(va_list args, const char *format, t_printf *env)
{
	if (format[env->i] == '.' && format[env->i + 1])
	{
		++env->i;
		if (format[env->i] == '*')
		{
			env->flag |= FLAG_PRECISION;
			env->precision = va_arg(args, int);
			++env->i;
		}
		else
		{
			while (format[env->i] == '0')
				++env->i;
			if (format[env->i] >= '1' && format[env->i] <= '9')
			{
				env->flag |= FLAG_PRECISION;
				env->precision = ft_atoip(&format[env->i]);
				while (format[env->i] >= '0' && format[env->i] <= '9')
					++env->i;
			}
			else
				env->flag |= FLAG_NO_PRECISION;
		}
	}
}
