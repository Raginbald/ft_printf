/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_o.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:05:14 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 12:31:17 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_arg_o(t_printf *env, va_list args)
{
	env->flag |= FLAG_UNBR;
	env->base = 8;
	ft_arg_needler(args, env, env->tab_qual, env->qualifier);
	env->len += ft_u_numberlen(env->num, env->base);
	env->str = ft_u_itoa_base(env, env->num, env->base);
	if (env->flag & FLAG_NO_PRECISION && env->num == 0)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}

void	ft_arg_oo(t_printf *env, va_list args)
{
	env->flag |= FLAG_UNBR;
	env->num = va_arg(args, unsigned long);
	env->base = 8;
	env->len += ft_u_numberlen(env->num, env->base);
	env->str = ft_u_itoa_base(env, env->num, env->base);
	if (env->flag & FLAG_NO_PRECISION && env->num == 0)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}
