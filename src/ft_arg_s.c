/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_s.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:18:15 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/23 13:18:04 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

static int	ft_get_wcharts_len(wchar_t *wstr, int nbr)
{
	int	i;
	int	j;

	i = 0;
	j = 1;
	while (wstr[i])
	{
		nbr -= ft_wclen_pf(wstr[i]);
		if (nbr < 0)
			return (j - 1);
		else if (nbr == 0)
			return (j);
		else
			++j;
		++i;
	}
	return (i);
}

static void	ft_arg_s_next(char *str, char padchar, t_printf *env)
{
	if (!(env->flag & FLAG_LEFT))
	{
		while (env->len < env->width--)
			env->printed += ft_putcharp(padchar);
	}
	env->printed += ft_putstr_len(str, env->len);
	while (env->len < env->width--)
		env->printed += ft_putcharp(' ');
}

void		ft_arg_s(t_printf *env, va_list args)
{
	char	*str;
	char	padchar;

	padchar = (env->flag & FLAG_ZERO) ? '0' : ' ';
	str = NULL;
	if (env->qualifier == 'l')
		ft_arg_ss(env, args);
	else
	{
		str = va_arg(args, char *);
		if (str == NULL)
			str = "(null)";
		env->len = ft_strlenp(str);
		if (env->precision != -1 && env->precision < 0)
			env->precision = -env->precision;
		if (env->precision != -1 && env->len > env->precision)
			env->len = env->precision;
		if (env->flag & FLAG_NO_PRECISION)
		{
			env->len = 0;
			str = NULL;
		}
		ft_arg_s_next(str, padchar, env);
	}
}

static void	ft_arg_ss_next(wchar_t *wstr, char padchar, t_printf *env)
{
	if (!(env->flag & FLAG_LEFT))
	{
		while (env->nbr_char < env->width--)
			env->printed += ft_putcharp(padchar);
	}
	if (env->flag & FLAG_PRECISION || env->flag & FLAG_NO_PRECISION)
	{
		if (env->flag & FLAG_PRECISION && env->width > 0)
			env->printed += ft_putwstr_len(wstr, env->len);
		else
			env->printed += ft_putwstr_len_pf(wstr, env->nbr_char);
	}
	else
		env->printed += ft_putwstr_len(wstr, env->len);
	while (env->nbr_char < env->width-- && env->width > 0)
		env->printed += ft_putcharp(' ');
}

void		ft_arg_ss(t_printf *env, va_list args)
{
	wchar_t	*wstr;
	char	padchar;

	padchar = (env->flag & FLAG_ZERO) ? '0' : ' ';
	wstr = va_arg(args, wchar_t *);
	if (wstr == NULL)
		wstr = L"(null)";
	env->len = ft_wstrlen(wstr);
	env->nbr_char = ft_wstrlen_pf(wstr);
	if (env->precision != -1 && env->precision < 0)
		env->precision = -env->precision;
	if (env->precision != -1 && env->nbr_char > env->precision)
		env->nbr_char = env->precision;
	if (env->flag & FLAG_NO_PRECISION)
	{
		env->nbr_char = 0;
		wstr = L"";
	}
	if (env->flag & FLAG_PRECISION && env->width > 0)
	{
		env->len = ft_get_wcharts_len(wstr, env->nbr_char);
		if (env->nbr_char != 1)
			env->nbr_char -= env->len;
	}
	ft_arg_ss_next(wstr, padchar, env);
}
