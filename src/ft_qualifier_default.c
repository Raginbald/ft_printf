/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_qualifier_default.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:32:34 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 11:32:35 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	ft_qualifier_default(t_printf *env, va_list args)
{
	if (env->flag & FLAG_SNBR)
	{
		env->snum = va_arg(args, int);
		if (env->snum < 0)
			env->flag |= FLAG_ISNEG;
	}
	else
		env->num = va_arg(args, unsigned int);
}
