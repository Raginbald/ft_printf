/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:31:56 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/23 13:19:22 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

static void		ft_init_next(t_arg_pf *tab)
{
	tab[0].op = 'd';
	tab[1].op = 'D';
	tab[2].op = 'i';
	tab[3].op = 'o';
	tab[4].op = 'O';
	tab[5].op = 'u';
	tab[6].op = 'U';
	tab[7].op = 'x';
	tab[8].op = 'X';
	tab[9].op = 'b';
	tab[10].op = 'c';
	tab[11].op = 'C';
	tab[12].op = 's';
	tab[13].op = 'S';
	tab[14].op = 'p';
	tab[15].op = '\0';
}

static t_arg_pf	*ft_init_tab_specifier(void)
{
	t_arg_pf	*tab;

	if (!(tab = (t_arg_pf *)malloc(sizeof(t_arg_pf) * 16)))
		ft_exitp(2, ERROR_MALLOC);
	tab[0].pf = ft_arg_d;
	tab[1].pf = ft_arg_dd;
	tab[2].pf = ft_arg_i;
	tab[3].pf = ft_arg_o;
	tab[4].pf = ft_arg_oo;
	tab[5].pf = ft_arg_u;
	tab[6].pf = ft_arg_uu;
	tab[7].pf = ft_arg_x;
	tab[8].pf = ft_arg_xx;
	tab[9].pf = ft_arg_b;
	tab[10].pf = ft_arg_c;
	tab[11].pf = ft_arg_cc;
	tab[12].pf = ft_arg_s;
	tab[13].pf = ft_arg_ss;
	tab[14].pf = ft_arg_p;
	tab[15].pf = ft_no_arg;
	ft_init_next(tab);
	return (tab);
}

static t_arg_pf	*ft_init_tab_qualifier(void)
{
	t_arg_pf	*tab;

	if (!(tab = (t_arg_pf *)malloc(sizeof(t_arg_pf) * 7)))
		ft_exitp(2, ERROR_MALLOC);
	tab[0].pf = ft_qualifier_default;
	tab[1].pf = ft_qualifier_hh;
	tab[2].pf = ft_qualifier_h;
	tab[3].pf = ft_qualifier_l;
	tab[4].pf = ft_qualifier_ll;
	tab[5].pf = ft_qualifier_j;
	tab[6].pf = ft_qualifier_z;
	tab[0].op = 'd';
	tab[1].op = 'H';
	tab[2].op = 'h';
	tab[3].op = 'l';
	tab[4].op = 'L';
	tab[5].op = 'j';
	tab[6].op = 'z';
	return (tab);
}

static void		ft_init_env(t_printf *env)
{
	env->i = 0;
	env->backup = 0;
	env->flag = 0;
	env->width = -1;
	env->precision = -1;
	env->qualifier = 0;
	env->tab_qual = ft_init_tab_qualifier();
	env->specifier = 0;
	env->tab_spec = ft_init_tab_specifier();
	env->base = 10;
	env->base_str = BASE_STR;
	env->printed = 0;
	env->nbr_char = 0;
	env->len = 0;
	env->str = 0;
	env->num = 0;
	env->snum = 0;
}

int				ft_printf(const char *format, ...)
{
	va_list		args;
	t_printf	env;

	va_start(args, format);
	ft_init_env(&env);
	while (format[env.i])
	{
		if (format[env.i] == '%')
		{
			++env.i;
			if (format[env.i] == '%')
				env.printed += ft_putcharp(format[env.i]);
			else if (format[env.i] != '\0')
				ft_print_argument(args, &env, format);
			else
				break ;
		}
		else
			env.printed += ft_putcharp(format[env.i]);
		++env.i;
	}
	va_end(args);
	return (env.printed);
}
