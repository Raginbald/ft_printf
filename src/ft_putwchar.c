/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/09 10:51:55 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/09 10:52:28 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int				ft_putwchar(wchar_t w)
{
	int				i;

	i = 0;
	if (w < 0x80)
		i += ft_putcharp(w);
	else if (w < 0x800)
	{
		i += ft_putcharp(0xC0 | (w >> 6));
		i += ft_putcharp(0x80 | (w & 0x3F));
	}
	else if (w < 0x10000)
	{
		i += ft_putcharp(0xE0 | (w >> 12));
		i += ft_putcharp(0x80 | ((w >> 6) & 0x3F));
		i += ft_putcharp(0x80 | (w & 0x3F));
	}
	else if (w < 0x200000)
	{
		i += ft_putcharp(0xF0 | (w >> 18));
		i += ft_putcharp(0x80 | ((w >> 12) & 0x3F));
		i += ft_putcharp(0x80 | ((w >> 6) & 0x3F));
		i += ft_putcharp(0x80 | (w & 0x3F));
	}
	return (i);
}
