/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:19:24 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/23 13:15:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_H
# define LIBFTPRINTF_H

# include <stddef.h>
# include <stdarg.h>
# include <unistd.h>
# include <inttypes.h>
# include <wchar.h>
# include "struct.h"
# include "define.h"

int		ft_putcharp(int c);
int		ft_putstrp(const char *str);
int		ft_putstr_len(const char *str, int len);
int		ft_putwstr(const wchar_t *str);
int		ft_putwstr_len(const wchar_t *wstr, int len);
int		ft_strlenp(const char *str);
int		ft_wstrlen(const wchar_t *str);
int		ft_putwchar(const wchar_t w);
int		ft_wclen_pf(wchar_t wc);
int		ft_wstrlen_pf(wchar_t *wstr);
int		ft_putwstr_len_pf(wchar_t *str, int len);
int		ft_atoip(const char *str);
int		ft_s_numberlen(long num, int base);
int		ft_u_numberlen(unsigned long num, int base);
char	*ft_u_itoa_base(t_printf *env, unsigned long num, int base);
char	*ft_s_itoa_base(t_printf *env, long num, int base);
int		ft_printf(const char *format, ...);
void	ft_print_argument(va_list args, t_printf *env, const char *format);
void	ft_print_buffer(t_printf *env);
void	ft_print_string(t_printf *env);
void	ft_print_number(t_printf *env);
void	ft_get_flags(const char *format, t_printf *env);
void	ft_get_width(va_list args, const char *format, t_printf *env);
void	ft_get_precision(va_list args, const char *format, t_printf *env);
void	ft_get_qualifier(const char *format, t_printf *env);
void	ft_get_specifier(const char *format, t_printf *env);
void	ft_arg_needler(va_list args, t_printf *env, t_arg_pf *tab, char c);
void	ft_qualifier_hh(t_printf *env, va_list args);
void	ft_qualifier_h(t_printf *env, va_list args);
void	ft_qualifier_l(t_printf *env, va_list args);
void	ft_qualifier_ll(t_printf *env, va_list args);
void	ft_qualifier_j(t_printf *env, va_list args);
void	ft_qualifier_z(t_printf *env, va_list args);
void	ft_qualifier_default(t_printf *env, va_list args);
void	ft_arg_s(t_printf *env, va_list args);
void	ft_arg_ss(t_printf *env, va_list args);
void	ft_arg_p(t_printf *env, va_list args);
void	ft_arg_d(t_printf *env, va_list args);
void	ft_arg_dd(t_printf *env, va_list args);
void	ft_arg_i(t_printf *env, va_list args);
void	ft_arg_o(t_printf *env, va_list args);
void	ft_arg_oo(t_printf *env, va_list args);
void	ft_arg_u(t_printf *env, va_list args);
void	ft_arg_uu(t_printf *env, va_list args);
void	ft_arg_x(t_printf *env, va_list args);
void	ft_arg_xx(t_printf *env, va_list args);
void	ft_arg_c(t_printf *env, va_list args);
void	ft_arg_cc(t_printf *env, va_list args);
void	ft_arg_b(t_printf *env, va_list args);
void	ft_no_arg(t_printf *env, va_list args);
void	ft_clear_env(t_printf *env);
void	ft_exitp(int fd, const char *str);
int		ft_maxp(int a, int b);

#endif
