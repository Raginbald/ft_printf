/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:19:33 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/23 13:17:14 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct s_printf	t_printf;
typedef struct s_arg_pf	t_arg_pf;

struct				s_printf
{
	int				i;
	int				backup;
	int				flag;
	int				width;
	int				precision;
	char			qualifier;
	t_arg_pf		*tab_qual;
	t_arg_pf		*tab_spec;
	char			specifier;
	int				base;
	char			*base_str;
	int				printed;
	int				nbr_char;
	int				len;
	char			*str;
	unsigned long	num;
	long			snum;
};

struct				s_arg_pf
{
	void			(*pf)(t_printf *, va_list);
	char			op;
};

typedef struct		s_itoa
{
	unsigned long	result;
	unsigned long	divisor;
	unsigned long	nbr;
	int				i;
	char			*buffer;
}					t_itoa;

#endif
