# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/07 11:18:54 by graybaud          #+#    #+#              #
#    Updated: 2015/09/21 15:32:49 by graybaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC 		= clang
NAME	= libftprintf.a
H		= includes/libftprintf.h \
			includes/struct.h

OBJ 	= $(SRC:.c=.o)
SRC 	= src/ft_printf.c 				\
			src/ft_print_argument.c 	\
			src/ft_atoip.c 				\
			src/ft_itoa_base.c 			\
			src/ft_maxp.c 				\
			src/ft_exitp.c 				\
			src/ft_get_flags.c 			\
			src/ft_get_width.c 			\
			src/ft_get_precision.c 		\
			src/ft_get_qualifier.c 		\
			src/ft_get_specifier.c 		\
			src/ft_qualifier_h.c 		\
			src/ft_qualifier_l.c 		\
			src/ft_qualifier_j.c 		\
			src/ft_qualifier_z.c 		\
			src/ft_qualifier_default.c 	\
			src/ft_arg_c.c 				\
			src/ft_arg_d.c	 			\
			src/ft_arg_o.c 				\
			src/ft_arg_p.c 				\
			src/ft_arg_b.c 				\
			src/ft_arg_s.c 				\
			src/ft_arg_u.c 				\
			src/ft_arg_x.c 				\
			src/ft_no_arg.c 			\
			src/ft_arg_needler.c 		\
			src/ft_number_len.c 		\
			src/ft_putcharp.c 			\
			src/ft_putwchar.c 			\
			src/ft_putstrp.c 			\
			src/ft_putwstr.c 			\
			src/ft_strlenp.c 			\
			src/ft_wstrlen.c 			\
			src/ft_clear_env.c 			\
			src/ft_print_number.c 		\
			src/ft_wclen_pf.c 			\
			src/ft_wstrlen_pf.c 		\
			src/ft_putwstr_len_pf.c

OBJ 	= $(SRC:.c=.o)
CFLAGS	= -Wall -Werror -Wextra -I./includes/
DFLAGS 	= -g
RM		= rm -rf

%.o: %.c $(H)
	@$(CC) $(CFLAGS) -I./includes/ -c $< -o $@

$(NAME): $(OBJ) $(H)
	@ar -q $(NAME) $(OBJ)
	@ranlib $(NAME)

all: $(NAME)
	@echo libftprintf done !

test: $(OBJ) $(H)
	@$(CC) -I./includes/ $(OBJ) -o test
	@echo tester done !
	@$(CC) -I./includes/ $(DFLAGS) $(SRC) -o debug
	@echo debugger done !

clean:
	@$(RM) $(OBJ)
	@echo clean done !

fclean: clean
	@$(RM) $(NAME) debug debug.dSYM test
	@echo fclean done !

re: fclean all
	@echo Re done !


reset: fclean test
	@echo Reset done !


.PHONY: all clean re fclean

